#include "../includes/font.h"

int initFont(TTF_Font **font, const char *filename, const int size) {
  if (TTF_Init() == -1) {
    fprintf(stderr, "Erreur TTF : %s\n", TTF_GetError());
    return 1;
  }

  if (!(*font = TTF_OpenFont(filename, size))) {
    fprintf(stderr, "Erreur TTF : %s\n", TTF_GetError());
    return 2;
  }

  return 0;
}

int writeText(GLuint *_textTexId, TTF_Font *font, const char *text,
              const SDL_Color color, const GLuint *backgroundTextureId) {
  SDL_Surface *d, *s;
  // Create text
  if (!(d = TTF_RenderUTF8_Blended_Wrapped(font, text, color, 0))) {
    freeFont(font);
    fprintf(stderr, "Erreur TTF : TTF_RenderUTF8_Blended_Wrapped\n");
    return 1;
  }

  // Surface finale
  if (!(s = SDL_CreateRGBSurface(0, d->w, d->h, 32, R_MASK, G_MASK, B_MASK,
                                 A_MASK))) {
    freeFont(font);
    fprintf(stderr, "Erreur SDL : %s\n", SDL_GetError());
    return 2;
  }

  if (backgroundTextureId) {
    // On garde la texture déjà présente
    assert(*backgroundTextureId);
    glBindTexture(GL_TEXTURE_2D, *backgroundTextureId);

    GLint w, h;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);

    // Surface qui va garder les données de la texture
    SDL_Surface *old;
    if (!(old = SDL_CreateRGBSurface(0, w, h, 32, R_MASK, G_MASK, B_MASK,
                                     A_MASK))) {
      freeFont(font);
      fprintf(stderr, "Erreur SDL : %s\n", SDL_GetError());
      return 2;
    }

    // Copie des données
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, old->pixels);

    // Scale les données sur la même taille que la surface finale
    SDL_BlitScaled(old, NULL, s, NULL);

    SDL_FreeSurface(old);

  } else {
    // Aucune texture existante, donc on la créée
    glGenTextures(1, _textTexId);
    assert(_textTexId);
  }

  glBindTexture(GL_TEXTURE_2D, *_textTexId);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  // Copie le texte sur la surface finale
  SDL_BlitSurface(d, NULL, s, NULL);
  SDL_FreeSurface(d);

  // Copie la surface dans la texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, s->w, s->h, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, s->pixels);

  SDL_FreeSurface(s);
  glBindTexture(GL_TEXTURE_2D, 0);
  return 0;
}

void freeFont(TTF_Font *font) {
  if (font) {
    TTF_CloseFont(font);
    font = NULL;
  }
}
