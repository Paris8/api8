#version 330

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec4 pos_model;
out vec3 vec_normal;
out vec4 shadowMapCoord;

uniform mat4 proj, model, view;
uniform mat4 lightView, lightProjection;

void main() {
  const mat4 bias = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);

  pos_model = model * vec4(pos, 1);
  vec_normal = normalize(transpose(inverse(model)) * vec4(normal, 0)).xyz;

  shadowMapCoord = bias * lightProjection * lightView * pos_model;
  gl_Position = proj * view * pos_model;
}
