#version 330

in vec3 vsoNormal;
in vec4 vsoModPos;

in vec2 vsoTexCoord;

out vec4 fragColor;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform mat4 viewMatrix;
uniform vec4 lumPos;
uniform float fadeAmount;

const float fadeStart = 0;
const float fadeEnd = 1;

void main(void) {
  const vec3 Vu = vec3(0, 0, -1);
  vec4 lumPosv = viewMatrix * lumPos;
  vec3 Ld = normalize(vsoModPos - lumPosv).xyz;
  vec3 reflexion = normalize(reflect(Ld, vsoNormal));
  float ispec = pow(clamp(dot(-Vu, reflexion), 0., 1.), 20);
  float phongIL = clamp(dot(-Ld, vsoNormal), 0., 1.);

  vec4 color1 = texture(tex0, vsoTexCoord);
  vec4 color2 = texture(tex1, vsoTexCoord);
  float fadeFactor = smoothstep(fadeStart, fadeEnd, fadeAmount / vsoTexCoord.x);

  fragColor = phongIL * mix(color1, color2, fadeFactor) + ispec * vec4(1);
}
