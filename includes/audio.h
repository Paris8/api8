#ifndef DEMO_AUDIO_H
#define DEMO_AUDIO_H 1

#include <SDL_mixer.h>

// Charge et lance la musique
void initMusic(Mix_Chunk *, const char *);

// Renvoie une moyenne de l'intensité du son
double averageMusic(void);

// Libère la musique en mémoire
void freeMusic(Mix_Chunk *);

#endif
