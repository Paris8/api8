#version 330

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float progress;

in vec2 vsoTexCoord;
out vec4 fragColor;

void main() {
  vec2 center = vec2(.5);
  vec2 zoomedTexCoord = mix(center, vsoTexCoord, progress);

  vec4 color1 = texture(tex0, vsoTexCoord);
  vec4 color2 = texture(tex1, zoomedTexCoord);

  fragColor = mix(color1, color2, smoothstep(0., 1., progress));
}
