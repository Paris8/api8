#include "../includes/audio.h"

// Stocke les données à propos de la musique
static double average;

// Channel son utilisé
static int _channel;

// Code utilisé en tant qu'erreur de SDL_Mixer
static int _errStatus = 2;

double averageMusic(void) { return average; }

void callback(void *_, Uint8 *stream, int size) {
  int i = 0;
  double tmp = 0.;
  for (signed short *stream16 = (signed short *)stream; i < size / 2; ++i) {
    tmp += stream16[i] / ((1 << 15) + 1.);
  }
  average = fabs(tmp / (i / 100.));
}

void initMusic(Mix_Chunk *music, const char *filename) {
  if (!Mix_Init(MIX_INIT_MID)) {
    fprintf(stderr, "Erreur Mix_Init - Chargement des librairies dynamiques\n");
    exit(_errStatus);
  }

  if (!Mix_GetSoundFonts()) {
    fprintf(stderr,
            "Aucun soundfonts, mettez les dans /usr/share/sounds/sf2\n");
    fprintf(stderr,
            "S'ils sont dans /usr/share/soundfonts, "
            "vous pouvez faire :\n"
            "sudo mkdir -p /usr/share/sounds/sf2 && "
            "sudo ln -s /usr/share/soundfonts/*.sf2 /usr/share/sounds/sf2/\n");
    exit(_errStatus);
  }

  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 1, 2048)) {
    fprintf(stderr, "Erreur Mix_OpenAudio\n");
    exit(_errStatus);
  };

  if (!(music = Mix_LoadWAV(filename))) {
    fprintf(stderr, "Erreur Mix_LoadWAV en chargant '%s'\n", filename);
    exit(_errStatus);
  }

  Mix_SetPostMix(callback, NULL);

  if ((_channel = Mix_PlayChannel(-1, music, -1)) == -1) {
    fprintf(stderr, "Erreur Mix_PlayChannel - Le son n'as pas pu être joué\n");
    exit(_errStatus);
  };
}

void freeMusic(Mix_Chunk *music) {
  if (music) {
    if (Mix_Playing(_channel)) {
      Mix_HaltChannel(_channel);
    }
    Mix_FreeChunk(music);
    music = NULL;
  }

  Mix_CloseAudio();
  Mix_Quit();
}
