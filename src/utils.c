#include "../includes/utils.h"

double get_dt(double *t0, const GLboolean override_t0) {
  double t = gl4dGetElapsedTime(), dt = (t - *t0) / 1000.0;
  if (override_t0) {
    *t0 = t;
  }

  return dt;
}

void bindAndLoadf(const char *name) {
  gl4duBindMatrix(name);
  gl4duLoadIdentityf();
}

void move(const GLfloat x, const GLfloat y, const GLfloat z) {
  gl4duTranslatef(x, y, z);
}

void rotateVec(GLfloat *vec, const GLfloat angle) {
  GLfloat y = vec[1];
  GLfloat z = vec[2];

  GLfloat cosTheta = (GLfloat)cos(angle);
  GLfloat sinTheta = (GLfloat)sin(angle);

  vec[1] = y * cosTheta - z * sinTheta;
  vec[2] = y * sinTheta + z * cosTheta;
}

void loadImg(const char *filename, const GLuint texture) {
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  SDL_Surface *s = NULL;
  if (!(s = IMG_Load(filename))) {
    fprintf(stderr, "Erreur de chargement de l'image (IMG_Load)\n"
                    "Using red color instead\n");
    GLuint p = 0xFF0000FF;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 &p);
  } else {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, s->w, s->h, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, s->pixels);
    SDL_FreeSurface(s);
  }

  glBindTexture(GL_TEXTURE_2D, 0);
}

void copyTexture(const GLuint src, const GLuint dst) {
  if (!(src || dst)) {
    fprintf(stderr,
            "Erreur copie texture: Source/Destination pas initalisée\n");
    exit(4);
  }

  glBindTexture(GL_TEXTURE_2D, src);
  GLint w, h;
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);

  SDL_Surface *s;
  if (!(s = SDL_CreateRGBSurface(0, w, h, 32, R_MASK, G_MASK, B_MASK,
                                 A_MASK))) {
    fprintf(stderr, "Erreur copie texture : %s\n", SDL_GetError());
    exit(4);
  }

  // Copie des données
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, s->pixels);

  // Copy data
  glBindTexture(GL_TEXTURE_2D, dst);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, s->w, s->h, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, s->pixels);

  SDL_FreeSurface(s);
  glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint getCurrentFramebufferID(void) {
  GLint currentFramebufferID = 0;
  glGetIntegerv(GL_FRAMEBUFFER_BINDING, &currentFramebufferID);

  return currentFramebufferID;
}
