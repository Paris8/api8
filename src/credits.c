#include "../includes/animations.h"

static GLuint _pId = 0;
static GLuint _texId = 0;
static GLuint _quadId = 0;

static const char matrix_modview[] = "modview";
static const char matrix_proj[] = "proj";

static void init(void);
static void draw(void);
static void deinit(void);

void credits(const int state) {
  switch (state) {
  case GL4DH_INIT:
    init();
    break;

  case GL4DH_DRAW:
    draw();
    break;

  case GL4DH_FREE:
    deinit();
    break;

  default:
    break;
  }
}

static void init(void) {
  _pId = gl4duCreateProgram("<vs>shaders/credits.vs", "<fs>shaders/credits.fs",
                            NULL);
  gl4duGenMatrix(GL_FLOAT, matrix_modview);
  gl4duGenMatrix(GL_FLOAT, matrix_proj);
  _quadId = gl4dgGenQuadf();

  // Code utilisé en tant qu'erreur de TTF_Font
  int errStatus = 1;

  // Charge la police
  TTF_Font *font = NULL;
  if (initFont(&font, "fonts/Instrument.ttf", 100)) {
    exit(errStatus);
  }

  // Ecrit avec la police sur une texture
  if (writeText(&_texId, font,
                "                    CRÉDITS\n"
                "Concours API8 — 7e édition\n\n"
                "- Font : fontesk et fontsquirrel\n"
                "- Audio : beepbox\n"
                "- Talent : aucun\n"
                "- Librairies : GL4D, SDL2 et extensions",
                (SDL_Color){255, 255, 255, 255}, NULL)) {
    exit(errStatus);
  }

  // Libère la police de la mémoire
  freeFont(font);
}

static void draw(void) {
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  static double t0 = -1;
  if (t0 == -1) {
    t0 = gl4dGetElapsedTime();
  }
  GLfloat dt = .25f * (GLfloat)get_dt(&t0, GL_FALSE) - 1.1f;

  GLfloat ratio = (GLfloat)_dims[0] / (GLfloat)_dims[1];
  bindAndLoadf(matrix_proj);
  gl4duFrustumf(-ratio, ratio, -1, 1, 2, 100);

  bindAndLoadf(matrix_modview);

  gl4duTranslatef(0.f, dt, -2.f);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _texId);
  gl4duScalef(1, 0.3f, 1);

  glUseProgram(_pId);
  gl4duSendMatrices();

  glUniform1i(glGetUniformLocation(_pId, "inv"), 1);
  glUniform1i(glGetUniformLocation(_pId, "tex"), 0);
  gl4dgDraw(_quadId);

  glUseProgram(0);
}

static void deinit(void) {
  if (_texId) {
    glDeleteTextures(1, &_texId);
    _texId = 0;
  }
}
