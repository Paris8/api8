#include "../includes/animations.h"

/* Mise-à-jour des animations en fonction du son */
static void update_with_audio(void (*)(int), void (*)(int), int);

static GLuint _transition_pId[2] = {0}, _transition_tId = 0, _quadId = 0,
              _transition_fbo[2] = {0};
static void transition_init(const char *, const int, GLuint *);
static void transition_draw(void (*)(int), void (*)(int), Uint32, Uint32, int,
                            int, GLuint *);
static void transition_deinit(GLuint *);

static void update_with_audio(void (*a0)(int), void (*a1)(int), int state) {
  if (a0) {
    a0(state);
  }

  if (a1) {
    a1(state);
  }
}

void transitionsInit(void) {
  if (!_quadId) {
    _quadId = gl4dgGenQuadf();
  }
  glGenFramebuffers(2, _transition_fbo);
}

void zoomIn(void (*a0)(int), void (*a1)(int), Uint32 t, Uint32 et,
            const int state) {
  const int gpu = 0;
  static GLuint tex[2];
  switch (state) {
  case GL4DH_INIT:
    transition_init("zoom_in", gpu, tex);
    break;

  case GL4DH_DRAW:
    transition_draw(a0, a1, t, et, state, gpu, tex);
    break;

  case GL4DH_FREE:
    transition_deinit(tex);
    break;

  case GL4DH_UPDATE_WITH_AUDIO:
    update_with_audio(a0, a1, state);
    break;

  default:
    break;
  }
}

static void transition_init(const char *fs_shader, const int gpu, GLuint *tex) {
  int vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);

  glGenTextures(2, tex);
  for (int i = 0; i < 2; ++i) {
    glBindTexture(GL_TEXTURE_2D, tex[i]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, vp[2], vp[3], 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, NULL);
  }
  char fragment_shader[256];
  sprintf(fragment_shader, "<fs>shaders/%s.fs", fs_shader);
  _transition_pId[gpu] =
      gl4duCreateProgram("<vs>shaders/basic.vs", fragment_shader, NULL);
}

static void transition_draw(void (*a0)(int), void (*a1)(int), Uint32 t,
                            Uint32 et, int state, int gpu, GLuint *tex) {
  glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                        GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
                                        (GLint *)&_transition_tId);
  GLint currentFramebufferID = getCurrentFramebufferID();

  glBindFramebuffer(GL_FRAMEBUFFER, _transition_fbo[0]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         tex[0], 0);
  if (a0) {
    a0(state);
  }

  glBindFramebuffer(GL_FRAMEBUFFER, _transition_fbo[1]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         tex[1], 0);
  if (a1) {
    a1(state);
  }

  glBindFramebuffer(GL_FRAMEBUFFER, currentFramebufferID);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         _transition_tId, 0);

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex[0]);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex[1]);

  glUseProgram(_transition_pId[gpu]);
  GLfloat progress = (GLfloat)et / (GLfloat)t;
  glUniform1f(glGetUniformLocation(_transition_pId[gpu], "progress"),
              (1.f - progress));
  glUniform1i(glGetUniformLocation(_transition_pId[gpu], "tex1"), 0);
  glUniform1i(glGetUniformLocation(_transition_pId[gpu], "tex0"), 1);
  gl4dgDraw(_quadId);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

static void transition_deinit(GLuint *tex) {
  if (tex[0]) {
    glDeleteTextures(2, tex);
    tex[0] = 0;
  }
  if (_transition_fbo[0]) {
    glDeleteFramebuffers(2, _transition_fbo);
    _transition_fbo[0] = 0;
  }
}

void genManifestant(struct manifestant *hero) {
  // Head and body
  hero->tete = gl4dgGenSpheref(7, 7);
  hero->corps = gl4dgGenCubef();

  // Arms
  hero->bras_d = gl4dgGenCubef();
  hero->bras_g = gl4dgGenCubef();

  // Legs
  hero->jambe_g = gl4dgGenCubef();
  hero->jambe_d = gl4dgGenCubef();
}

void drawManifestant(const char *matrix_model, const struct manifestant *hero,
                     const float deplacement, const GLboolean oscillate) {
  const GLfloat oscillation = oscillate ? ((float)sin(2 * deplacement) + 1) : 0;
  const float x = hero->ox + deplacement;
  const float z = hero->oz * 4;

  // Draw corps
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 0.5f, 0.2f);
  gl4duTranslatef(0, 3, -1);
  move(x, 0, hero->oz);

  gl4duSendMatrices();
  gl4dgDraw(hero->corps);

  // Draw head
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 0.2f, 0.2f);
  gl4duTranslatef(0, 10.9f, -1);
  move(x, 0, hero->oz);

  gl4duSendMatrices();
  gl4dgDraw(hero->tete);

  // Draw leg left
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 1, 0.05f);
  gl4duTranslatef(0, 0, -1);
  move(x, 0, z);
  gl4duRotatef(-oscillation * 90, 0, 0, 1);

  gl4duSendMatrices();
  gl4dgDraw(hero->jambe_g);

  // Draw leg right
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 1, 0.05f);
  gl4duTranslatef(0, 0, -7);
  move(x, 0, z);
  gl4duRotatef(oscillation * 90, 0, 0, 1);

  gl4duSendMatrices();
  gl4dgDraw(hero->jambe_d);

  // Draw arm left
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 0.4f, 0.05f);
  gl4duTranslatef(0, 4, 1);
  move(x, 0, z);
  gl4duRotatef(oscillation * 90, 0, 0, 1);

  gl4duSendMatrices();
  gl4dgDraw(hero->bras_g);

  // Draw arm right
  bindAndLoadf(matrix_model);
  gl4duScalef(0.1f, 0.4f, 0.05f);
  gl4duTranslatef(0, 4, -8);
  move(x, 0, z);
  gl4duRotatef(-oscillation * 90, 0, 0, 1);

  gl4duSendMatrices();
  gl4dgDraw(hero->bras_d);
}

void twisting(void (*a0)(int), void (*a1)(int), Uint32 t, Uint32 et,
              const int state) {
  const int gpu = 1;
  static GLuint tex[2];
  switch (state) {
  case GL4DH_INIT:
    transition_init("twister", gpu, tex);
    break;

  case GL4DH_DRAW:
    transition_draw(a0, a1, t, et, state, gpu, tex);
    break;

  case GL4DH_FREE:
    transition_deinit(tex);
    break;

  case GL4DH_UPDATE_WITH_AUDIO:
    update_with_audio(a0, a1, state);
    break;

  default:
    break;
  }
}
