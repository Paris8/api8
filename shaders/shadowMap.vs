
#version 330

uniform mat4 model;
uniform mat4 lightView;
uniform mat4 lightProjection;

layout(location = 0) in vec3 vsiPosition;

void main(void) {
  gl_Position = lightProjection * lightView * model * vec4(vsiPosition, 1.0);
}
