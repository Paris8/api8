#version 330

layout(location = 0) in vec3 vsiPosition;
layout(location = 1) in vec3 vsiNormal;
layout(location = 2) in vec2 vsiTexCoord;

out vec2 vsoTexCoord;
out vec3 vsoNormal;
out vec4 vsoModPos;
out float vsoIntensiteLum;

uniform mat4 proj, model, view;

uniform vec4 lumPos;
uniform float phase;
uniform float averageMusic;

void main(void) {
  mat4 N = transpose(inverse(view * model));
  vsoNormal = normalize((N * vec4(vsiNormal, 0)).xyz);
  vec4 lumPosv = view * lumPos;

  float dist = length(vsiPosition.xz), freq = 5, amplitude = averageMusic * (sqrt(2.) - dist);
  float y = amplitude * cos(phase + freq * dist);
  vec3 p = vec3(vsiPosition.x, y, vsiPosition.z);

  vsoModPos = view * model * vec4(p, 1);
  vec3 Ld = normalize(vsoModPos - lumPosv).xyz;

  vsoIntensiteLum = clamp(dot(-Ld, vsoNormal), 0., 1.);
  gl_Position = proj * vsoModPos;

  vsoTexCoord = vec2(vsiTexCoord.x, 1 - vsiTexCoord.y);
}
