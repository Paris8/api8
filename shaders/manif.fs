#version 330

in vec4 pos_model;
in vec3 vec_normal;
in vec4 shadowMapCoord;

out vec4 fragColor;

const int max = 3; // should change in cpu too

uniform vec4 couleur;
uniform vec4 lumPos[max];
uniform mat4 view;
uniform vec4 lumColor[max];
uniform sampler2D shadowmap;

void main() {
  const vec3 vue = vec3(0, 0, -1);
  const vec4 soleil_color = vec4(0.81f, 0.78f, 0.66f, 1.0f);

  for(int i = 0; i < max; i++) {
    vec3 torche = normalize(pos_model.xyz - lumPos[i].xyz);
    float intensite_lumineuse = clamp(dot(vec_normal, -torche), 0., 1.);

    vec3 shadowMapPcoord = shadowMapCoord.xyz / shadowMapCoord.w;
    if(texture(shadowmap, shadowMapPcoord.xy).r < shadowMapPcoord.z) {
      intensite_lumineuse = 0;
    }

    vec3 reflet = (transpose(inverse(view)) * vec4(reflect(torche, vec_normal), 0)).xyz;
    float intensite_specularite = pow(clamp(dot(reflet, -vue), 0., 1.), 10);

    vec4 color = intensite_specularite * lumColor[i] + 0.1 * soleil_color + 0.9 * intensite_lumineuse * couleur;
    if(i == 0) {
      fragColor = color;
    } else {
      fragColor = mix(fragColor, color, 1. * (i / 1));
    }
  }
}
