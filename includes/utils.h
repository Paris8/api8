#ifndef DEMO_UTILS_H
#define DEMO_UTILS_H 1

#include <GL4D/gl4dh.h>
#include <GL4D/gl4duw_SDL2.h>
#include <SDL_image.h>
#include <string.h>

// Récupère un delta-temps
double get_dt(double *, const GLboolean);

// Bind and load a matrix
void bindAndLoadf(const char *name);

/* Move something in space
 *
 * `+x` => movement to right
 *
 * `-x` => movement to left
 *
 * `+y` => movement to top (fly)
 *
 * `-y` => movement to bottom (sink)
 *
 * `+z` => move closer
 *
 * `-z` => move away */
void move(const GLfloat x, const GLfloat y, const GLfloat z);

// Rotate a vector XYZ
void rotateVec(GLfloat *, const GLfloat);

// Load an image into a GL texture
void loadImg(const char *filename, const GLuint texture);

// Copy a texture
void copyTexture(const GLuint, const GLuint);

// Get the current FBO ID
GLuint getCurrentFramebufferID(void);

#endif
