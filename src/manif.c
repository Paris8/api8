#include "../includes/animations.h"

static GLuint _pId[2] = {0};
static GLuint _planId = 0;

#define HEROS_NUMBER 30 // should change in fragment shader too
static struct manifestant _herosId[HEROS_NUMBER];

#define SHADOW_MAP_SIDE 1024
static GLuint _fbo = 0, _shadow_map_tex = 0;

static const char *matrix_proj = "proj";
static const char *matrix_model = "model";
static const char *matrix_view = "view";
static const char *matrix_lview = "lightView";
static const char *matrix_lproj = "lightProjection";

static void init(void);
static void draw(void);
static void deinit(void);

void manif(const int state) {
  switch (state) {
  case GL4DH_INIT:
    init();
    break;

  case GL4DH_DRAW:
    draw();
    break;

  case GL4DH_FREE:
    deinit();
    break;

  default:
    break;
  }
}

static void init(void) {
  _planId = gl4dgGenQuadf();

  // Last Offset X and Z
  for (int i = 0; i < HEROS_NUMBER; ++i) {
    genManifestant(&_herosId[i]);

    // Coordinates
    _herosId[i].ox = ((GLfloat)rand() / (GLfloat)RAND_MAX) * -50;
    _herosId[i].oz = 30 - ((GLfloat)rand() / (GLfloat)RAND_MAX) * 50;
  }
  _pId[0] =
      gl4duCreateProgram("<vs>shaders/manif.vs", "<fs>shaders/manif.fs", NULL);

  _pId[1] = gl4duCreateProgram("<vs>shaders/shadowMap.vs",
                               "<fs>shaders/shadowMap.fs", NULL);

  gl4duGenMatrix(GL_FLOAT, matrix_proj);
  gl4duGenMatrix(GL_FLOAT, matrix_model);
  gl4duGenMatrix(GL_FLOAT, matrix_view);
  gl4duGenMatrix(GL_FLOAT, matrix_lview);
  gl4duGenMatrix(GL_FLOAT, matrix_lproj);

  // Création et paramétrage de la Texture de shadow map
  glGenTextures(1, &_shadow_map_tex);
  glBindTexture(GL_TEXTURE_2D, _shadow_map_tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_MAP_SIDE,
               SHADOW_MAP_SIDE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

  // Création du FBO
  glGenFramebuffers(1, &_fbo);
}

static void draw(void) {
  static double t0 = 0;
  double dt = get_dt(&t0, GL_TRUE);
  static float deplacement = 0;

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glClearColor(0.2f, 0.2f, 0.8f, 1); // couleur ciel

  glUseProgram(_pId[1]);

  bindAndLoadf(matrix_lproj);
  gl4duLoadIdentityf();
  gl4duOrthof(-13, 13, -6, 6, 1, 40);

  bindAndLoadf(matrix_lview);
  gl4duLookAtf(9, 6, 0, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
  glCullFace(GL_FRONT);

  GLint currentFramebufferID = getCurrentFramebufferID();

  glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                         _shadow_map_tex, 0);
  glViewport(0, 0, SHADOW_MAP_SIDE, SHADOW_MAP_SIDE);
  glClear(GL_DEPTH_BUFFER_BIT);
  for (int i = 0; i < HEROS_NUMBER; ++i) {
    drawManifestant(matrix_model, &_herosId[i], deplacement, GL_TRUE);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, currentFramebufferID);

  glCullFace(GL_BACK);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);

  glViewport(0, 0, _dims[0], _dims[1]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glCullFace(GL_FRONT_FACE);
  glUseProgram(_pId[0]);

  const GLfloat couleur_plan[] = {0.3f, 0.3f, 0.3f, 1},
                couleur_heros[] = {1, 1, 0, 1};
  GLint couleur_gpu = glGetUniformLocation(_pId[0], "couleur");

  bindAndLoadf(matrix_proj);
  GLfloat ratio = (GLfloat)_dims[0] / (GLfloat)_dims[1];
  gl4duFrustumf(-1, 1, -ratio, ratio, 1, 1000);

  bindAndLoadf(matrix_view);
  const GLfloat distance = 5; // distance from the scene
  gl4duLookAtf(0, distance, distance, 0, 0, 0, 0, 1, 0);
  // gl4duRotatef(-90, 0, 1, 0); // rotation camera

  bindAndLoadf(matrix_model);
  gl4duRotatef(-90, 1, 0, 0);
  gl4duScalef(4 * distance, 2 * distance, 1);

  gl4duSendMatrices();
  glUniform4fv(couleur_gpu, 1, couleur_plan);
  gl4dgDraw(_planId);

  GLfloat lumpos[HEROS_NUMBER][4], lumcolor[HEROS_NUMBER][4];
  glUniform4fv(couleur_gpu, 1, couleur_heros);
  for (int i = 0; i < HEROS_NUMBER; ++i) {
    // Torchs
    // Position
    float offset = 2; // we make sure that lights never eats the ground
    GLfloat lumpos_i[] = {(_herosId[i].ox / 10) + (deplacement / 10), 1,
                          ((_herosId[i].oz / 20) - 1) - offset};
    rotateVec(lumpos_i, ((float)sin(deplacement) + 1));
    memcpy(lumpos[i], lumpos_i, sizeof(lumpos_i));

    // Color
    const GLfloat lumcolor_red[] = {1, 0, 0, 1};
    memcpy(lumcolor[i], lumcolor_red, sizeof(lumcolor_red));

    drawManifestant(matrix_model, &_herosId[i], deplacement, GL_TRUE);
  }

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _shadow_map_tex);
  int max_gpu = 3; // HEROS_NUMBER;
  glUniform4fv(glGetUniformLocation(_pId[0], "lumPos"), max_gpu,
               (float *)lumpos);
  glUniform4fv(glGetUniformLocation(_pId[0], "lumColor"), max_gpu,
               (float *)lumcolor);
  glUniform1i(glGetUniformLocation(_pId[0], "shadowmap"), 0);

  deplacement += (float)(0.4 * M_PI * dt);
  glUseProgram(0);

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
}

static void deinit(void) {
  if (_fbo) {
    glDeleteTextures(1, &_shadow_map_tex);
    glDeleteFramebuffers(1, &_fbo);
    _fbo = 0;
  }
}
