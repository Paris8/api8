#ifndef DEMO_FONT_H
#define DEMO_FONT_H 1

#include <GL4D/gl4dh.h>
#include <SDL_ttf.h>

/* Charge une police
 *
 * Renvoie 1 en cas de problème d'initialisation
 *
 * Renvoie 2 en cas de problème de chargement de la police
 *
 * Renvoie 0 en cas de succès */
int initFont(TTF_Font **, const char *, const int);

/* Ecris un texte sur une texture
 *
 * Renvoie 1 en cas de problème d'écriture sur la surface
 *
 * Renvoie 2 en cas de problème de surface avec la SDL
 *
 * Renvoie 0 en cas de succès */
int writeText(GLuint *, TTF_Font *, const char *, const SDL_Color,
              const GLuint *);

// Libère une police de la mémoire
void freeFont(TTF_Font *);

#endif
