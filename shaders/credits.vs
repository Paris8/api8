#version 330

layout(location = 0) in vec3 vsiPosition;
layout(location = 2) in vec2 vsiTexCoord;
uniform int inv;
uniform mat4 modview, proj;
out vec2 vsoTexCoord;

void main(void) {
  gl_Position = proj * modview * vec4(vsiPosition, 1);
  if(inv != 0) {
    vsoTexCoord = vec2(vsiTexCoord.s, 1 - vsiTexCoord.t);
  } else {
    vsoTexCoord = vec2(vsiTexCoord.s, vsiTexCoord.t);
  }
}
