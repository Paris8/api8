# Makefile
#  Auteur : Farès BELHADJ
#  Email  : amsi@up8.edu
#  Date   : 28/04/2020
#  Modif  : Anri KENNEL (anri.kennel@etud.univ-paris8.fr) - 27/05/2023

# Définition des commandes utilisées
CC       = gcc
ECHO     = echo
RM       = rm -f
TAR      = tar
ZIP      = zip
MKDIR    = mkdir
CHMOD    = chmod
CP       = rsync -R
MINIFIER = minifier
CURL     = curl -s
DU       = du --bytes --human-readable
TAIL     = tail
GREP     = grep
AWK      = awk
STRIP    = strip -s
UPX      = upx

# Déclaration des options du compilateur
COPTI    = -O3 -flto
CFLAGS   = -Wall $(COPTI) -std=gnu17
CPPFLAGS = -I.
LDFLAGS  = -lm

# Définition des fichiers et dossiers
PACKNAME   = api8_2023
PROGNAME   = demo
VERSION    = anri
distdir    = $(PACKNAME)_$(PROGNAME)-$(VERSION)
HEADERS    = $(wildcard includes/*.h)
SOURCES    = main.c $(wildcard src/*.c)
MSVCSRC    = $(patsubst %,<ClCompile Include=\"%\\\" \\/>,$(SOURCES))
OBJ        = $(SOURCES:.c=.o)
DOXYFILE   = documentation/Doxyfile
VSCFILES   = $(PROGNAME).vcxproj $(PROGNAME).sln
EXTRAFILES = COPYING $(wildcard shaders/*.?s images/*.png fonts/*.ttf audio/*.mid) $(VSCFILES)
DISTFILES  = $(SOURCES) Makefile $(HEADERS) $(DOXYFILE) $(EXTRAFILES)

# API 8
MAXSIZE = 64

# Traitements automatiques pour ajout de chemins et options (ne pas modifier)
ifneq (,$(shell ls -d /usr/local/include 2>/dev/null | tail -n 1))
	CPPFLAGS += -I/usr/local/include
endif
ifneq (,$(shell ls -d $(HOME)/local/include 2>/dev/null | tail -n 1))
	CPPFLAGS += -I$(HOME)/local/include
endif
ifneq (,$(shell ls -d /usr/local/lib 2>/dev/null | tail -n 1))
	LDFLAGS += -L/usr/local/lib
endif
ifneq (,$(shell ls -d $(HOME)/local/lib 2>/dev/null | tail -n 1))
	LDFLAGS += -L$(HOME)/local/lib
endif
ifeq ($(shell uname),Darwin)
	MACOSX_DEPLOYMENT_TARGET = 11.0
	CFLAGS += -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
	LDFLAGS += -framework OpenGL -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
else
	LDFLAGS += -lGL
endif

CPPFLAGS += $(shell sdl2-config --cflags)
LDFLAGS  += -lGL4Dummies $(shell sdl2-config --libs) -lSDL2_mixer -lSDL2_image -lSDL2_ttf

all: $(PROGNAME)
all: strip
all: upx

$(PROGNAME): $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $(PROGNAME)

debug: CFLAGS := $(filter-out $(COPTI),$(CFLAGS)) -Wextra -Wconversion -g3 -Og \
				-Wno-sign-conversion -Wno-unused-parameter -Wno-unused-function \
				-Wdouble-promotion -Wshadow -Wcast-align -Wstrict-prototypes \
				-fanalyzer -fsanitize=undefined -fsanitize-undefined-trap-on-error
debug: $(PROGNAME)

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

strip:
	$(STRIP) $(PROGNAME)

upx:
	$(UPX) $(PROGNAME)

dist: distdir
	$(CHMOD) -R a+r $(distdir)
	$(TAR) zcvf $(distdir).tgz $(distdir)
	$(RM) -r $(distdir)

zip: distdir
	$(CHMOD) -R a+r $(distdir)
	$(ZIP) -r $(distdir).zip $(distdir)
	$(RM) -r $(distdir)

distdir: $(DISTFILES)
	$(RM) -r $(distdir)
	$(MKDIR) $(distdir)
	$(CHMOD) 777 $(distdir)
	$(CP) $(DISTFILES) $(distdir)

doc: $(DOXYFILE)
	cat $< | sed -e "s/PROJECT_NAME *=.*/PROJECT_NAME = $(PROGNAME)/" | \
		sed -e "s/PROJECT_NUMBER *=.*/PROJECT_NUMBER = $(VERSION)/" >> $<.new
	mv -f $<.new $<
	cd documentation && doxygen && cd ..

msvc: $(VSCFILES)
	@echo "Now these files ($?) already exist. If you wish to regenerate them, you \
					should first delete them manually."

$(VSCFILES):
	@echo "Generating $@ ..."
	@cat ../../Windows/templates/gl4dSample$(suffix $@) | \
		sed -e "s/INSERT_PROJECT_NAME/$(PROGNAME)/g" | \
		sed -e "s/INSERT_TARGET_NAME/$(PROGNAME)/" | \
		sed -e "s/INSERT_SOURCE_FILES/$(MSVCSRC)/" > $@

clean:
	@$(RM) -r $(PROGNAME) $(OBJ) *~ $(distdir).tgz $(distdir).zip $(distdir)/ \
		gmon.out core.* documentation/*~ shaders/*~ documentation/html

distminified: distdirminified dist

# Remove all unnecessary extra files
distdirminified: EXTRAFILES := $(filter-out $(VSCFILES),$(EXTRAFILES))
distdirminified: DISTFILES := $(filter-out $(DOXYFILE),$(DISTFILES))
distdirminified: distdir
	@$(CURL) https://raw.githubusercontent.com/BaseMax/C-Minifier/main/Minifier.c | \
		$(CC) -o $(MINIFIER) $(CFLAGS) -x c -
	$(foreach f,$(SOURCES),./$(MINIFIER) $(f) $(distdir)/$(f);)
	@$(RM) $(MINIFIER)

size: $(PROGNAME)
size: SIZEFILES = $(PROGNAME) $(filter-out $(VSCFILES) COPYING,$(EXTRAFILES))
size: SIZEINFOS = ("; printf $$1; print "/$(MAXSIZE)Ko - soit \
			`$(shell echo $(SIZEFILES) | sed 's/ /`, `/g')`)"
size:
	@$(DU) --total $(SIZEFILES) | \
		$(TAIL) -n 1 | \
		$(GREP) 'K' | \
		$(GREP) -o '[0-9]*'| \
		$(AWK) '{ \
			if($$1 <= $(MAXSIZE)) { \
				printf "> Taille conforme $(SIZEINFOS) \
			} else { \
				printf "> Fichiers trop lourd $(SIZEINFOS) \
			} \
		}'
