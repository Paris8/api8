# Démo API 8 de 2023

Auteur :

- Anri Kennel

## Idée

Idée de base : Faire une manifestation et un "tag"

## Information sur la compilation

Lancer `make size` pour avoir la taille finale, le projet ne doit pas dépasser
64 Ko, car il suit la [track 64 Ko de API8 2023](https://api8.fr/?sec=1#64K). <!-- https://api8.fr/?sec=2023 -->

> Exemple de compilation + vérification de taille
>
> ```sh
> make clean demo strip upx size
> ```
>
> 1. nettoie le dossier courant
> 2. compile le projet
> 3. utilise l'utilitaire `strip`
> 4. compresse avec l'outil `upx`
> 5. vérifie la taille finale du projet

## Dépendances

- `SDL2_mixer` (utilisation de fichier MID: `timidity++`
  (avec `fluidsynth` et `soundfont-fluid`)) (+ facultativement `libsamplerate`)
  > Il faut bien vérifier que les soundfonts sont installés dans
  > le dossier `/usr/share/sounds/sf2`, exemple :
  >
  > ```bash
  > sudo mkdir -p /usr/share/sounds/sf2
  > sudo ln -s /usr/share/soundfonts/*.sf2 /usr/share/sounds/sf2/
  > ```
- `SDL2_ttf`
- `SDL2_image`
