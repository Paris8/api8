#include <time.h>

#include "includes/animations.h"

// Son de fond
static Mix_Chunk *_ambiance = NULL;

// Dimensions initiales de la fenêtre
GLuint _dims[] = {1280, 720};

static void init(void);
static void closure(void);

int main(const int argc, char *argv[]) {
  if (!gl4duwCreateWindow(argc, argv, "Demo API8 2023", GL4DW_POS_CENTERED,
                          GL4DW_POS_CENTERED, _dims[0], _dims[1],
                          GL4DW_OPENGL | GL4DW_SHOWN)) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre.\n");
    return 1;
  }

  // Animations
  GL4DHanime animations[] = {{10000, manif, NULL, NULL},     // Manifestation
                             {2000, manif, tag, zoomIn},     // Transition
                             {10000, tag, NULL, NULL},       // Tag
                             {2000, tag, credits, twisting}, // Transition
                             {9500, credits, NULL, NULL},    // Crédits
                             {0, NULL, NULL, NULL}};

  gl4dhInit(animations, _dims[0], _dims[1], init);
  atexit(closure);
  gl4duwDisplayFunc(gl4dhDraw);
  gl4duwMainLoop();

  return 0;
}

static void init(void) {
  initMusic(_ambiance, "audio/ambiance.mid");

  if (SDL_GL_SetSwapInterval(-1)) {
    fprintf(stderr, "Erreur VSync : %s\n", SDL_GetError());
  }

  srand((Uint32)time(NULL));

  transitionsInit();

  printf("Bienvenue dans la démo API8 !\n");
}

static void closure(void) {
  freeMusic(_ambiance);
  gl4duClean(GL4DU_ALL);

  printf("Merci du visionnage !\n");
}
