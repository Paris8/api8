#version 330

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float progress;

in vec2 vsoTexCoord;
out vec4 fragColor;

// Fonction de déformation
vec2 deform(float it) {
  vec2 center = vec2(0.5);
  vec2 offset = vsoTexCoord - center;

  // Torsion
  float angle = offset.y * it * radians(180) * 2;
  float cosTheta = cos(angle);
  float sinTheta = sin(angle);
  vec2 twistedOffset = vec2(cosTheta * offset.x - sinTheta * offset.y, sinTheta * offset.x + cosTheta * offset.y);

  // Vague
  float wave = sin(twistedOffset.y * 20);
  vec2 deformedUV = twistedOffset + vec2(wave * it, 0);

  return center + deformedUV;
}

void main() {
  vec4 color1 = texture(tex0, deform(progress));
  vec4 color2 = texture(tex1, deform(1. - progress));

  fragColor = mix(color1, color2, smoothstep(0., 1., progress));
}
