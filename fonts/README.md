Lien de la police : https://fontesk.com/instrument-sans-typeface/
⇾ Sous licence OFL, j'ai choisi `InstrumentSans-Regular.ttf`.

Compressé avec : https://www.fontsquirrel.com/tools/webfont-generator
Pour les paramètres, voir : generator_config.txt
