#include "../includes/animations.h"

static GLuint _pId = 0;
static GLuint _murId = 0;
static GLuint _texId[2] = {0};

static const char *matrix_proj = "proj";
static const char *matrix_model = "model";
static const char *matrix_view = "view";

static void init(void);
static void draw(void);
static void deinit(void);

void tag(const int state) {
  switch (state) {
  case GL4DH_INIT:
    init();
    break;

  case GL4DH_DRAW:
    draw();
    break;

  case GL4DH_FREE:
    deinit();
    break;

  default:
    break;
  }
}

static void init(void) {
  _murId = gl4dgGenGrid2df(100, 100);
  _pId = gl4duCreateProgram("<vs>shaders/tag.vs", "<fs>shaders/tag.fs", NULL);

  glGenTextures(2, _texId);
  loadImg("images/wall.png", _texId[0]);
  copyTexture(_texId[0], _texId[1]);

  TTF_Font *font = NULL;
  if (initFont(&font, "fonts/Instrument.ttf", 100)) {
    exit(3);
  }
  if (writeText(&_texId[1], font, "macron = loser", (SDL_Color){0, 0, 0, 255},
                &_texId[0])) {
    exit(3);
  }
  freeFont(font);

  gl4duGenMatrix(GL_FLOAT, matrix_proj);
  gl4duGenMatrix(GL_FLOAT, matrix_model);
  gl4duGenMatrix(GL_FLOAT, matrix_view);
}

static void draw(void) {
  static double t0 = 0;
  float dt = (float)get_dt(&t0, GL_TRUE);

  static GLfloat angle = 0.0f;
  GLfloat lum_pos[] = {-2.0f *
                           (float)cos(angle * (GLfloat)M_PI / (5.0f * 180.0f)),
                       0.2f, 0.0f, 1.0f};
  const GLfloat distance = 2; // distance from the scene

  glEnable(GL_DEPTH_TEST);
  glClearColor(0.2f, 0.2f, 0.2f, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(_pId);

  bindAndLoadf(matrix_proj);
  GLfloat ratio = (GLfloat)_dims[0] / (GLfloat)_dims[1];
  gl4duFrustumf(-1, 1, -ratio, ratio, 2, 1000);

  bindAndLoadf(matrix_view);
  gl4duLookAtf(0, distance, distance, 0, 0, 0, 0, 1, 0);
  // gl4duRotatef(-90, 0, 1, 0); // rotation camera

  bindAndLoadf(matrix_model);
  gl4duRotatef(45, 1, 0, 0);
  gl4duRotatef(180, 0, 1, 0);
  gl4duRotatef(20, 0, 0, 1);
  gl4duScalef(-1, 1, 1);

  angle += dt * 90.0f;
  gl4duSendMatrices();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _texId[0]);
  glUniform1f(glGetUniformLocation(_pId, "phase"), 0);
  glUniform4fv(glGetUniformLocation(_pId, "lumPos"), 1, lum_pos);
  glUniform1f(glGetUniformLocation(_pId, "averageMusic"),
              (float)averageMusic() * 5);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, _texId[1]);
  GLfloat fade_amout = ((GLfloat)t0 / 7000.f) - 1.5f;
  glUniform1f(glGetUniformLocation(_pId, "fadeAmount"), fade_amout);
  glUniform1i(glGetUniformLocation(_pId, "tex0"), 0);
  glUniform1i(glGetUniformLocation(_pId, "tex1"), 1);

  gl4dgDraw(_murId);

  glUseProgram(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_DEPTH_TEST);
}

static void deinit(void) {
  if (_texId[0]) {
    glDeleteTextures(sizeof(_texId) / sizeof(*_texId), _texId);
    _texId[0] = 0;
  }
}
