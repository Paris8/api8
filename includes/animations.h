#ifndef DEMO_ANIMATION_H
#define DEMO_ANIMATION_H 1

#include "audio.h"
#include "font.h"
#include "utils.h"

// Dimensions de la fenêtre
extern GLuint _dims[];

// Scène de manifestation
void manif(const int);

// Scène du tag sur le mur
void tag(const int);

// Crédits de fin
void credits(const int);

// Initialisation des transitions
void transitionsInit(void);

// Transition zoom
void zoomIn(void (*)(int), void (*)(int), Uint32, Uint32, const int);

// Transition qui se déforme
void twisting(void (*)(int), void (*)(int), Uint32, Uint32, const int);

// Personnage
struct manifestant {
  GLuint corps, tete, jambe_g, jambe_d, bras_g, bras_d;
  // Offsets
  GLfloat ox, oz;
};

// Génère un manifestant
void genManifestant(struct manifestant *);

// Dessine un manifestant
void drawManifestant(const char *, const struct manifestant *, const float,
                     const GLboolean);

#endif
